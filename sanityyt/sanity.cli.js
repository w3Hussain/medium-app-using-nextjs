import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'jmeg9gde',
    dataset: 'production'
  }
})
